function lcs(string1, string2){

	let larger, smaller, largerLength, smallerLength;
	if(string1.length >= string2.length){
		larger = string1;
		smaller = string2;
	} else{
		larger = string2;
		smaller = string1;
	}
	largerLength = larger.length;
	smallerLength = smaller.length;
	let matrix = Array.from({length: smallerLength+1}, ()=>Array.from({length: largerLength+1},() => 0));
	for(let row = 1; row < matrix.length; row ++){
		for (let col=1; col<matrix[row].length; col++){
			if(larger[col-1] === smaller[row-1]){
				matrix[row][col] = matrix[row-1][col-1] + 1
			} else {
				matrix[row][col] = Math.max(matrix[row][col-1], matrix[row-1][col])
			}
		}
	}
	let traceResult = [];
	(function tracer(row,col){
		if(matrix[row][col] === 0) return
		if(matrix[row][col] === matrix[row][col-1]){
				tracer(row, col-1)
			} else if (matrix[row][col] === matrix[row-1][col]){
				tracer(row-1, col)
			} else if(matrix[row][col] === matrix[row-1][col-1] + 1){
				traceResult.push(larger[col-1])
				tracer(row-1,col-1)
			}
	})
	(smallerLength,largerLength)
	return traceResult.reverse().join('')
}